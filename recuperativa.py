#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import collections
import os

# Lee el archivo
def cargarArchivo(): 
    with open('iris.json', 'r') as archivo:
        datos = json.load(archivo)
    return datos
    
#se imprimen los nombres de las especies
def imprimirNombre(nomEspecies):
    for j in nomEspecies:
        print "Especie: ", j
        
#se obtienen los datos de las especies y sus derivados nombres
def obtenerNombres(datos):
    nomEspecies = []
    for i in datos:
		# Revisa keys y value al mismo tiempo
        for clave, valores in i.items(): 
            if clave == "species":
				# Nombres en el arreglo
                if valores in nomEspecies: 
                    pass

                elif valores not in nomEspecies: 
					# Se agregan los nombres
                    nomEspecies.append(valores) 

    print("Las especies en el documento son:")
    imprimirNombre(nomEspecies)
    
#se definen las variables
def definirpromedio(datos):
	count=0
	count2=0
	count3=0
	temp=0
	temp2=0
	prom_s=0
	prom_v=0
	prom_vi=0
	prom_s1=0
	prom_v1=0
	prom_vi1=0
	
	#recibe keys y valores y recorre cada valor 
	for i in datos:
		for clave, valores in  i.items():
			if clave== "petalLength":
				#se guarda en variable temporal el largo
				temp=valores
			if clave=="petalWidth":
				#se guarda en variable temporal el ancho
				temp2=valores
			if clave == "species":
				
				#se realizan las operaciones para el promedio en cada
				#una de las especies
				if valores == "setosa":
					count=count + 1
					prom_s=temp + prom_s
					prom_s1= temp2 + prom_s1
					temp=0
					temp2=0
					
				if valores == "versicolor":
					count2 = count2 + 1
					prom_v = temp + prom_v
					prom_v1 = temp2 + prom_v1
					temp=0
					temp2=0
										
				if valores == "virginica":
					count3 = count3 +1
					prom_vi = temp + prom_vi
					prom_vi1 = temp2 + prom_vi1
					temp=0
					temp2=0
					
	#se almacenan los valores de altos y anchos de cada especie				
	altos=[]
	anchos=[]
	altos.append(prom_s/count)
	altos.append(prom_v/count2) 	
	altos.append(prom_vi/count3)	
	anchos.append(prom_s1/count)
	anchos.append(prom_v1/count2)
	anchos.append(prom_vi1/count3)
	
	#se imprimen los valores del promedio co su respectivo valor	
	print ("\nEl promedio del ancho y alto de los pétalos por especie son: ")		
	print ("El alto de setosa es: ", altos[0], " y el ancho es: ", anchos[0]) 
	print ("El alto de versicolor es: ", altos[1], " y el ancho es: ", anchos[1])
	print ("El alto de virginica es: ", altos[2], " y el ancho es: ", anchos[2])
	
	
	print ("\nLa especie que tiene en promedio los pétalos más anchos y más altos es: ")
	#se  imprime la especie de mayor alto y mayor ancho
	if (max(altos)== altos[0] and max(anchos)== anchos[0]):
		print("La especie setosa es la que tiene los petalos mas anchos de: ",anchos[0]," y altos de: ",altos[0])
	if (max(altos)== altos[1] and max(anchos)== anchos[1]):
		print("La especie versicolor es la que tiene los petalos mas anchos de: ",anchos[1]," y altos de: ",altos[1])
	if (max(altos)== altos[2] and max(anchos)== anchos[2]):
		print("La especie virginica es la que tiene los petalos mas anchos de: ",anchos[2]," y altos de: ",altos[2])
		print ("\n")
	#se retorna la lista con los altos y anchos de cada especie
	return altos, anchos
	
#variable donde se calculara el rango a travez del promedio de cada
#especie
def rango(datos, altos, anchos):
	temp=0
	temp2=0
	count_setosa=0
	count_versicolor=0
	count_virginica=0
	
	#se recorren los datos uno a uno
	for i in datos:
		for clave, valores in i.items():
			#se guarda en variable temporal el largo
			if clave== "petalLength":
				temp=valores
			#se guarda en variable temporal el ancho
			if clave=="petalWidth":
				temp2=valores
			if clave == "species":
				
				#se evaluan el ancho y el alto de cada especie 
				if ((valores == "setosa")and (altos[0]+3>temp and temp>altos[0]-3)and (anchos[0]+3>temp2 and anchos[0]-3<temp2)):
					count_setosa=count_setosa + 1
					temp=0
					temp2=0
				if ((valores == "versicolor")and (altos[1]+3>temp and temp>altos[1]-3)and (anchos[1]+3>temp2 and anchos[1]-3<temp2)):
					count_versicolor=count_versicolor + 1
					temp=0
					temp2=0
				if ((valores == "virginica")and (altos[2]+3>temp and temp>altos[2]-3)and (anchos[2]+3>temp2 and anchos[2]-3<temp2)):
					count_virginica=count_virginica + 1
					temp=0
					temp2=0
					
					
	#se imprimen la cantidad de registros que cumplen con la condicion anterior
	print ("\nSe encuentran los siguientes rangos en cada una de las especies: ")
	print ("La cantidad de registros de setosas que estan en el rango de alto ",altos[0]-3," - ",altos[0]+3," y en el rango de anchos ",anchos[0]-3, " - ",anchos[0]+3," y en total son: ",count_setosa)
	print ("La cantidad de registros de versicolor que estan en el rango de alto ",altos[1]-3," - ",altos[1]+3," y en el rango de anchos ",anchos[1]-3, " - ",anchos[1]+3," y en total son: ",count_versicolor)
	print ("La cantidad de registros de virginicas que estan en el rango de alto ",altos[2]-3," - ",altos[2]+3," y en el rango de anchos ",anchos[2]-3, " - ",anchos[2]+3," y en total son: ",count_virginica)
	print ("\n")
#variable que a partir de los valores del promedio verificara cual es
#mayor que las otras
def recibedatos(datos):
	temp=0
	count_s=0	
	count_v=0
	count_vi=0
	for i in datos:
		
		for clave, valores in i.items():
			if clave== "sepalLength":
				temp=valores
			#se guarda los largos de sepal en una variable temporal
			if clave == "species":
					if valores == "setosa":
						if count_s<temp:
							count_s=temp
						
					if valores == "versicolor":
						if count_v<temp:
							count_v=temp
							
					if valores == "virginica":
						if count_vi<temp:
							count_vi=temp
	#en un arreglo almacenamos los largos de sepal por especie
	sepalLength=[]
	sepalLength.append(count_s)
	sepalLength.append(count_v)
	sepalLength.append(count_vi)
	
	
	print("\nLa medida máxima para el alto de un sépalo lo contiene: ")
	#se evalua que especie tiene el valor de largo mas alto
	if (max(sepalLength)== sepalLength[0]):
		print("La especie setosa es la que tiene el sepalo mas alto corresponde a ",sepalLength[0])
	
	if (max(sepalLength)== sepalLength[1]):
		print("La especie versicolor es la que tiene el sepalo mas alto corresponde a ",sepalLength[1])
	
	if (max(sepalLength)== sepalLength[2]):
		print("La especie virginica es la que tiene el sepalo mas alto corresponde a",sepalLength[2])
		print("\n")
	
	#se crea el archivo.json para setosa
def creararchivo(datos):
	concatenar=[]
	archivo = open("setosa.json", "w")
	#se leen los datos
	for i in datos:
		#se almacena al inicio una llave
		concatenar.extend("{")
		for clave, valores in i.items():
			#se almacena la tupla en la lista concatenar			
			concatenar.append(clave+" : "+str(valores))
			if clave == "species":
					if valores == "setosa":
						p=concatenar
					#se almacena en la variable p, todos los registros 
					#de setosa						
					if valores == "versicolor":
						concatenar=[]
					if valores== "virginica":
						concatenar=[]
		#se agrega el cierre de llave al final de cada tupla
		concatenar.extend("}")
	#se omiten los ultimos 6 registros
	b=p[:-6]
	#se formatea en json la variable a 
	a=json.dumps(b)
	#se escribe en el archivo la variable a
	archivo.write(str(a))
	#se cierra el archivo
	archivo.close()
	#por medio de expresiones regulares, se formatea el archivo en formato json
	os.system("vi -c ':%s/"+'"'+"{"+'"'+",/{/g' -c ':%s/"+', "'+"}"+'"'+"/}/g' -c ':%s/ : /"+'"'+" : "+'"'+"/g' -c ':x!' setosa.json")	
	
def creararchivo1(datos):
	concatenar=[]
	archivo = open("versicolor.json", "w")
	#se leen los datos
	for i in datos:
		#se almacena al inicio una llave
		concatenar.extend("{")
		for clave, valores in i.items():		
			
			concatenar.append(clave+" : "+str(valores))
			if clave == "species":
					if valores == "setosa":
						concatenar=[]						
					if valores == "versicolor":
						#se almacena en la variable p, todos los registros 
						#de versicolor
						p=concatenar
					if valores== "virginica":
						concatenar=[]
		concatenar.extend("}")
	#se omiten los ultimos 6 registros
	b=p[1:-6]
	#se formatea en json la variable a 
	a=json.dumps(b)
	#se escribe en el archivo la variable a
	archivo.write(str(a))
	#se cierra el archivo
	archivo.close()
	#por medio de expresiones regulares, se formatea el archivo en formato json
	os.system("vi -c ':%s/"+'"'+"{"+'"'+",/{/g' -c ':%s/"+', "'+"}"+'"'+"/}/g' -c ':%s/ : /"+'"'+" : "+'"'+"/g' -c ':x!' versicolor.json")
		
def creararchivo2(datos):
	concatenar=[]
	archivo = open("virginica.json", "w")
	#se leen los datos
	for i in datos:
		#se almacena al inicio una llave
		concatenar.extend("{")
		for clave, valores in i.items():		
			
			concatenar.append(clave+" : "+str(valores))
			if clave == "species":
					if valores == "setosa":
						concatenar=[]						
					if valores == "versicolor":
						concatenar=[]
					if valores== "virginica":
	#se almacena en la variable p, todos los registros de virginica
						p=concatenar
		concatenar.extend("}")
	#se leen los valore desde el 2do en adelante
	b=p[1:]
	#se formatea en json la variable a 
	a=json.dumps(b)
	#se escribe en el archivo la variable a
	archivo.write(str(a))
	#se cierra el archivo
	archivo.close()
	#por medio de expresiones regulares, se formatea el archivo en formato json
	os.system("vi -c ':%s/"+'"'+"{"+'"'+",/{/g' -c ':%s/"+', "'+"}"+'"'+"/}/g' -c ':%s/ : /"+'"'+" : "+'"'+"/g' -c ':x!' virginica.json")	

if __name__ == '__main__':
	#variables utilizadas
    altos =[] 
    anchos=[]
    datos = cargarArchivo() 
    obtenerNombres(datos)
    altos, anchos=definirpromedio(datos)
    rango(datos, altos, anchos)
    recibedatos(datos)
    creararchivo(datos)
    creararchivo1(datos)
    creararchivo2(datos)
    print ("\nPD: Ya se crearon los archivos.json en el directorio")